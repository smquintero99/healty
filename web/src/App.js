import { Anchor, Box, Button, DataTable, Grommet, Heading, Menu, Meter, Text } from 'grommet'
import { Close, Edit } from 'grommet-icons'
import { AppBar } from './Widgets/Layout'
import { grommet } from 'grommet/themes'

import React, { Component, Fragment } from 'react'
import { db, fire } from './db'
import './App.css'


const map_seconds = arr => arr.map(({ seconds: s}) => s)
const deconstruct_array = (arr, zero) => map_seconds(arr).map(s => s - zero)
const create_breath_array = ({ inhalations, exhalations, rests, retentions }) => [
  ...map_seconds(inhalations), 
  ...map_seconds(exhalations), 
  ...map_seconds(rests), 
  ...map_seconds(retentions)
]

const order_breath_array = arr => create_breath_array(arr).sort() 
const calculate_length_of_array = arr => 
  order_breath_array(arr)[order_breath_array(arr).length - 1] - order_breath_array(arr)[0]

export default class App extends Component{
  state = {
    data: [],
    average_bradth_percent: 0
  }

  componentDidMount(){
    let smokes = []
    fire.collection('Meditations').limit(10).get()
    .then( docs => {
      docs.forEach(doc => smokes.push(this.wrangle_doc(doc.data())) )
      const smokes_length = smokes.map(({ length_of_smoke: l }) => l)
      const longest_breadth = Math.max(...smokes_length)
      const average_bradth_percent = Math.round((smokes_length.reduce((d, i) => 
        d + i, 0)/smokes_length.length)*(100/longest_breadth)
      )

      const data = smokes.map(({ inhalations, rests, retentions, exhalations, 
        created: c, length_of_smoke: l, exhaled, inhaled, retained, rested 
      }) => ({ date:c, inhalations, retentions, rests, exhalations, exhaled, inhaled, 
        retained, rested, percent: Math.round( 100* l/longest_breadth)
      }))

      this.setState({ data, average_bradth_percent }, () => console.log(this.state))     
    })
    .catch(console.log)
  }

  wrangle_doc = ({inhalations, exhalations, rests, retentions, created}) => ({
    inhalations: inhalations.length, exhalations: exhalations.length,
    rests: rests.length, retentions: retentions.length,
    created: new Date(created.seconds*1000).toLocaleString('en-US'),
    full_array: create_breath_array({ inhalations, exhalations, rests, retentions }),
    sorted_array: order_breath_array({ inhalations, exhalations, rests, retentions }),
    length_of_smoke: calculate_length_of_array({ inhalations, exhalations, rests, retentions }),
    exhaled: deconstruct_array(exhalations, inhalations[0].seconds), 
    inhaled: deconstruct_array(inhalations, inhalations[0].seconds),
    retained: deconstruct_array(retentions, inhalations[0].seconds),
    rested: deconstruct_array(rests, inhalations[0].seconds)
  })

  save_docs = () => db.collection('Smokes').insertOne({'test': 'test'}).then(console.log).catch(console.log)

  render(){
    const { data, average_bradth_percent } = this.state

    const columns = [
      { property: 'date', header: 'Date', align: 'center', footer: 'Total'}, 
      { property: 'inhalations', header: 'Inhalations', align: 'center', 
        aggregate: 'sum', footer: { aggregate: true }
      }, { property: 'retentions', header: 'Retentions', align: 'center', 
        aggregate: 'sum', footer: { aggregate: true }
      }, { property: 'exhalations', header: 'Exhalations', align: 'center', 
        aggregate: 'sum', footer: { aggregate: true }
      }, { property: 'rests', header: 'Rests', align: 'center', 
        aggregate: 'sum', footer: { aggregate: true }
      }, { property: 'duration', header: 'Duration', 
        render: ({ percent, date }) => <Box pad={{ vertical: 'xsmall' }}>
            <Meter values={[
              { value: date !== 'Total' ? percent : average_bradth_percent }
              ]} thickness="small" size="small" />
          </Box>          
      }, { property: 'duplicate', header: 'Duplicate', align: 'end',
        render: ({date}) => date !== 'Total' 
          ? <Fragment>
              <Button 
                icon={<Edit size='medium' style={{marginBottom:-5}} />} 
                style={{padding:9, marginRight:16}} 
                onClick={() => {}} 
              />
              <Button icon={<Close size='small'/>} style={{ padding:9 }} onClick={() => {}} primary/>
            </Fragment>
          : <Anchor href="#" primary label="Save" color="light-1" onClick={this.save_docs}/>
        }
    ]

    return (
      <Grommet theme={grommet} full>
        <AppBar>
          <Heading level='3' margin='none' style={{ fontSize:33 }}>Healty</Heading>
          <Menu label={<Text>Menú</Text>} items={[]} />
        </AppBar>
        <Box
          direction='row' 
          overflow={{ horizontal: 'hidden', vertical: 'hidden' }} 
          style = {{minHeight:'100vh', paddingBottom:'20vh'}} 
          background='linear-gradient(0.5turn, #FFFFFF, #E2E2E2)'
        >
          <Box style={{margin:'auto'}}>
            <DataTable 
              groupBy="inhalations"
              columns = { columns } 
              data = { data } 
              sortable
              background = {{
                header: "light-5",
                body: ["light-1", "light-3"],
                footer: 'dark-1'
              }}
            />
          </Box>
        </Box>
      </Grommet>
    )  
  }
}


