import { Stitch, RemoteMongoClient } from 'mongodb-stitch-browser-sdk'
import firebase from 'firebase'

export const client = Stitch.initializeDefaultAppClient('live-xosht')
export const db = client.getServiceClient(RemoteMongoClient.factory, 'live-db').db('zen')

export const fire_config = {
    apiKey: "AIzaSyC0TqC2uXriC3Ade3O9XB96fGia-h2wbww",
    authDomain: "react-c41e5.firebaseapp.com",
    databaseURL: "https://react-c41e5.firebaseio.com",
    projectId: "react-c41e5",
    storageBucket: "react-c41e5.appspot.com",
    messagingSenderId: "316886447982",
    appId: "1:316886447982:web:e23769a23f549829fdb37f"
}

firebase.initializeApp(fire_config)
export const fire = firebase.firestore()
