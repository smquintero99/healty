import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:zen/stats.dart';
import 'dart:math';

class AnimatedButton extends StatefulWidget {
  @override
  _AnimatedButtonState createState() => _AnimatedButtonState();
}

class _AnimatedButtonState extends State<AnimatedButton> with SingleTickerProviderStateMixin {
  double _scale;
  bool _isInhaling;
  AnimationController _controller;
  List _inhalations;
  List _retentions;
  List _exhalations;
  List<DateTime> _rests;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1200),
      lowerBound: 0.0,
      upperBound: 1,
    )..addListener(() {
        setState(() {});
    });

    _isInhaling = true;
    _inhalations = [];
    _retentions = [];
    _exhalations = [];
    _rests = [];
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onTapDown(TapDownDetails details) {
    _controller.duration = Duration(milliseconds: 3000);
    if(_isInhaling == true){
      _inhalations.add(DateTime.now());
      _controller.forward();
    } else {
      _exhalations.add(DateTime.now());
      _controller.reverse();
    }
    _isInhaling = !_isInhaling;
  }

  void _onTapUp(TapUpDetails details) {
    if(_isInhaling != true){
      _retentions.add(DateTime.now());
    } else {
      _rests.add(DateTime.now());
    }
  }

  void _onDrag(DragEndDetails details) {
    print(details.velocity.pixelsPerSecond.dx > 500);
    if(details.velocity.pixelsPerSecond.dx > 500){
      Firestore.instance.runTransaction((transaction) async {
        await transaction.set(
          Firestore.instance.collection("Meditations").document(), {
            'created': DateTime.now(),
            'inhalations': _inhalations,
            'retentions':  _retentions,
            'exhalations': _exhalations,
            'rests': _rests
          }
        );
      }).then((onDoc){    
        Navigator.push(context, MaterialPageRoute(builder: (context) => StatsScreen(
            _inhalations, _retentions, _exhalations, _rests
        )));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _controller.value*-3;

    return GestureDetector(
      onTapDown: _onTapDown,
      onTapUp: _onTapUp,
      onHorizontalDragEnd: _onDrag,
      child: Transform.scale(
        scale: _scale,
        child: _animatedButtonUI,
      ),
    );
  }

  Animatable<Color> background = TweenSequence<Color>([
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Color(0x80000000),
        end: Colors.red[900]
      ),
    ),
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Colors.red[900],
        end: Colors.deepOrangeAccent[700]
      ),
    ),
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Colors.deepOrangeAccent[700],
        end: Colors.red
      ),
    ),
  ]);

  Widget get _animatedButtonUI => Container(
    height: 200,
    width: 200,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(200),
      boxShadow: [
        BoxShadow(
          color: background.evaluate(
            AlwaysStoppedAnimation(
              min(_controller.value, 1)
            )
          ),
          blurRadius: 60.0,
        ),
      ],
    )
  );
}
