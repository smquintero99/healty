import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter/material.dart';
import 'package:zen/main.dart';
class StatsScreen extends StatefulWidget {
  final List _inhalations;
  final List _retentions;
  final List _exhalations;
  final List _rests;
  StatsScreen(
    this._inhalations, this._exhalations, this._retentions, this._rests, 
    {Key key}
  ) : super(key: key);

  @override
  _StatsPageState createState() => _StatsPageState();

}

class _StatsPageState extends State<StatsScreen> {

  void _onDrag(DragEndDetails details) {
    print(widget._inhalations);
    print(widget._retentions);
    print(widget._exhalations);
    print(widget._rests);
    if(details.velocity.pixelsPerSecond.dx < -500){
        Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()));
    }
  }

  calculateDuration(L, D){ 
    print(widget._inhalations);
    print(widget._retentions);
    print(widget._exhalations);
    print(widget._rests);

    print(L);
    print(D);
    try {
      return L.asMap().map((l,i) =>  MapEntry(
      l,
      D.length == L.length || l < L.length - 1
        ? i.difference(D[l]).inMilliseconds
        : 0,
      )).values.toList().reduce((a, b) => a + b) 
      / ((D.length == L.length ? L.length : L.length - 1)*1000);
    } catch(e) {
      return 0;
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      body: GestureDetector(
        onHorizontalDragEnd: _onDrag,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Center(
                    child: OutlineButton(
                      child: Column(
                        children: <Widget>[
                          new Text(
                            widget._inhalations.length.toString(),
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 48),
                          ),
                          new Text(
                            'Inhalations',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 16),
                          ),
                        ]
                      ),
                      onPressed: () {},
                      shape: new CircleBorder(),
                      padding: EdgeInsets.all(30.0),
                      borderSide: BorderSide(color: Colors.orange, width: 2),
                    ),
                  ),  
              ),
              Expanded(
                flex: 2,
                child:  Column(
                  children: <Widget>[
                    Text(
                      calculateDuration(widget._retentions, widget._inhalations).toStringAsFixed(2),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 32),
                    ),
                    Text(
                      'seg',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.orange, fontSize: 24),
                    ),
                  ]
                )
              ),
            ]
          ),
          SizedBox(height: 50),
          Row(
            children: <Widget>[
              Expanded(
                flex: 3,
                child:  Column(
                  children: <Widget>[
                    Text(
                      calculateDuration(widget._exhalations, widget._inhalations).toStringAsFixed(2),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 32),
                    ),
                    Text(
                      'seg',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.lightBlue, fontSize: 24),
                    ),
                  ]
                )
              ),
              Expanded(
                flex: 3,
                child:  Column(
                  children: <Widget>[
                    Text(
                      calculateDuration(widget._rests, widget._exhalations).toStringAsFixed(2),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 32),
                    ),
                    Text(
                      'seg',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.orange, fontSize: 24),
                    ),
                  ]
                )
              ),
              Expanded(
                flex: 3,
                child:  Column(
                  children: <Widget>[
                    Text(
                      calculateDuration(widget._inhalations.skip(1).toList(), widget._rests).toStringAsFixed(2),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 32),
                    ),
                    Text(
                      'seg',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.lightBlue, fontSize: 24),
                    ),                  
                  ]
                )
              )
            ])
          ]),
        )
      ),
      backgroundColor: Colors.black,
    );
  }
}
